create database phone;

use phone;

create table numbers (
 name varchar(50),
 phone varchar(50)
);

insert into numbers VALUES("Ibad Haider", "123-4567");

insert into numbers VALUES("Random Dude", "987-5433");

insert into numbers VALUES("Fantastique", "123-1241");

